golang-github-libgit2-git2go-v32 (32.1.9-1~bpo11+1) bullseye-backports-staging; urgency=medium

  * Rebuild for bullseye-backports-staging.

 -- Pirate Praveen <praveen@debian.org>  Thu, 31 Mar 2022 18:03:07 +0530

golang-github-libgit2-git2go-v32 (32.1.9-1) experimental; urgency=medium

  * New upstream version 32.1.9

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Mar 2022 13:27:25 +0530

golang-github-libgit2-git2go-v32 (32.1.4-2) experimental; urgency=medium

  * Rebuild with newer embedtls

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Mar 2022 01:17:13 +0530

golang-github-libgit2-git2go-v32 (32.1.4-1~bpo11+1) bullseye-backports-staging; urgency=medium

  * Rebuild for bullseye-backports-staging.

 -- Pirate Praveen <praveen@debian.org>  Tue, 14 Dec 2021 16:13:44 +0530

golang-github-libgit2-git2go-v32 (32.1.4-1) experimental; urgency=medium

  * Rename package to match go.mod
  * New upstream version 32.1.4
  * Update XS-Go-Import-Path: github.com/libgit2/git2go/v32
  * Update minimum version of libgit2
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Sat, 11 Dec 2021 21:29:42 +0530

golang-gopkg-libgit2-git2go.v31 (31.4.3-4) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.6.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sun, 29 Aug 2021 22:18:38 +0530

golang-gopkg-libgit2-git2go.v31 (31.4.3-3~bpo11+1) bullseye-backports-staging; urgency=medium

  * Rebuild for bullseye-backports-staging.

 -- Pirate Praveen <praveen@debian.org>  Thu, 13 May 2021 00:24:05 +0530

golang-gopkg-libgit2-git2go.v31 (31.4.3-3) experimental; urgency=medium

  * Switch to github.com/libgit2/git2go/v31 as import path (Closes: #979315)
    Its only reverse build dependency (gitaly) already switched to new path.

 -- Pirate Praveen <praveen@debian.org>  Mon, 08 Mar 2021 15:59:04 +0530

golang-gopkg-libgit2-git2go.v31 (31.4.3-2) unstable; urgency=medium

  * Upload to unstable.
  * Source-only upload for migration.
  * Bump debhelper-compat to 13

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 14 Dec 2020 18:44:29 +0530

golang-gopkg-libgit2-git2go.v31 (31.4.3-1) experimental; urgency=medium

  * New upstream version 31.4.3
    - Rename the source and binary packages (from v30 to v31)
  * Tighten dependency on libgit2

 -- Utkarsh Gupta <utkarsh@debian.org>  Tue, 08 Dec 2020 17:22:41 +0530

golang-gopkg-libgit2-git2go.v30 (30.3.2-2) unstable; urgency=medium

  * Source-only upload for migration.
  * Bump Standards-Version to 4.5.

 -- Utkarsh Gupta <utkarsh@debian.org>  Tue, 08 Dec 2020 16:48:33 +0530

golang-gopkg-libgit2-git2go.v30 (30.3.2-1) unstable; urgency=medium

  * New upstream version 30.3.2 (Closes: #976522)
    - Rename the source and binary packages (from v28 to v30)
  * Refresh d/patches
  * Update dependencies
    - Tighten dependency on libgit2
    - Add new dependencies, golang-golang-x-crypto-dev and
      golang-github-google-shlex-dev, and git.

 -- Utkarsh Gupta <utkarsh@debian.org>  Mon, 07 Dec 2020 13:55:43 +0530

golang-gopkg-libgit2-git2go.v28 (0.28.5-1) unstable; urgency=medium

  * New upstream version 0.28.5
  * Drop patch as it is merged upstream

 -- Utkarsh Gupta <utkarsh@debian.org>  Sat, 29 Feb 2020 18:13:29 +0530

golang-gopkg-libgit2-git2go.v28 (0.28.4-1) unstable; urgency=medium

  * New upstream version 0.28.4
  * Update d/warch for doing sane updates
  * Fix d/control wrt cme
  * Refresh patches

 -- Utkarsh Gupta <utkarsh@debian.org>  Sun, 16 Feb 2020 21:34:17 +0530

golang-gopkg-libgit2-git2go.v28 (0.28+git20190813.37e5b53-3) unstable; urgency=medium

  * Reupload to unstable (libgit2 0.28 is in unstable now)
  * Drop compat file, rely on debhelper-compat
  * Bump Standards-Version to 4.4.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 03 Dec 2019 18:12:36 +0530

golang-gopkg-libgit2-git2go.v28 (0.28+git20190813.37e5b53-2) experimental; urgency=medium

  * Team upload
  * Fix XS-Go-Import-Path

 -- Sruthi Chandran <srud@debian.org>  Thu, 26 Sep 2019 15:17:11 +0530

golang-gopkg-libgit2-git2go.v28 (0.28+git20190813.37e5b53-1) experimental; urgency=medium

  * Team upload
  * New upstream version 0.28+git20190813.37e5b53 (Closes: #931697)
      + Rename the source and binary packages (from v27 to v28)
      + Patch for fixing invalid version guard (from v27 to v28)
  * debian: Bump up the debhelper version to 12
  * d/control: Bump up the policy standard version to 4.4.0
  * d/changelog: Fix lintian P: file-contains-trailing-whitespace
  * d/watch: Add watch file

 -- Jongmin Kim <jmkim@pukyong.ac.kr>  Sat, 17 Aug 2019 17:22:19 +0900

golang-gopkg-libgit2-git2go.v27 (0.27+git20180529.9abc050-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 12 Jul 2018 09:51:48 +0530

golang-gopkg-libgit2-git2go.v27 (0.27+git20180529.9abc050-1) experimental; urgency=medium

  * New upstream version 0.27+git20180529.9abc050 (Closes: #898890)
  * All tests are passing (Closes: #897522)
  * Add myself to uploaders
  * Use XS-Go-Import-Path

 -- Pirate Praveen <praveen@debian.org>  Sat, 16 Jun 2018 12:46:30 +0530

golang-gopkg-libgit2-git2go.v26 (0.26+git20170903.0.eb0bf21-1) sid; urgency=medium

  [ Michael Stapelberg ]
  * gbp.conf: set debian-branch
  * update debian/gitlab-ci.yml (using salsa.debian.org/go-team/ci/cmd/ci)

  [ Maximiliano Curia ]
  * Prepare v26 version (Closes: 876707)
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sat, 03 Mar 2018 11:29:13 +0100

golang-git2go (0.24+git20161003.0.2209188-1) unstable; urgency=medium

  * New upstream release.
  * Use debhelper 10
  * Set the minimum version of the libgit2 required
  * Bump Standards-Version to 4.0.1.
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Tue, 22 Aug 2017 13:10:35 +0200

golang-git2go (0.0~git20150731.0.ed62fda-1) unstable; urgency=medium

  * New upstream snapshot.
  * Add debian/patches/0001-disable-remote-tests.patch (Closes: #792218)

 -- Michael Stapelberg <stapelberg@debian.org>  Mon, 03 Aug 2015 09:05:32 +0200

golang-git2go (0.0~git20150623-1) unstable; urgency=medium

  * Initial release.

 -- Maximiliano Curia <maxy@debian.org>  Sun, 28 Jun 2015 23:12:05 +0200
